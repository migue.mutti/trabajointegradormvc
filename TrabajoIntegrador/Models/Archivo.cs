﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrabajoIntegrador.Models
{
    public class Archivo
    {
        

        [Required(ErrorMessage ="El campo {0} es requerido")]
        public string Name { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(5)]
        [EmpezarConPunto(ErrorMessage ="La extension debe iniciar con un punto")]
        public string Extension { get; set; }

        public Archivo() { }

        public Archivo(string nombre, string ext) {
            this.Name = nombre;
            this.Extension = ext;

        }
    }
    
}
