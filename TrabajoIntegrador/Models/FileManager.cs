﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TrabajoIntegrador.Models
{
    public class FileManager
    {
        string Repo = @"E:\Temp";
    
        public List<Archivo> getAllFiles()
        {
            List<Archivo> lista = new List<Archivo>();

            string[] nombreArchivos = Directory.GetFiles(this.Repo);

            foreach (var nombrearchivo in nombreArchivos)
            {
                Archivo archivo = new Archivo();
                archivo.Name = Path.GetFileNameWithoutExtension(nombrearchivo);
                archivo.Extension = Path.GetExtension(nombrearchivo);
                lista.Add(archivo);
            }

            return lista;
        }

        internal void Edit(Archivo archivoInicial, Archivo archivoModificado)
        {
            try
            {
                string pathIni = Path.Combine(this.Repo, archivoInicial.Name + archivoInicial.Extension);
                string pathNuevo = Path.Combine(this.Repo, archivoModificado.Name + archivoModificado.Extension);
                File.Move(pathIni, pathNuevo);
            }
            
            catch (Exception)
            {
               throw;
            }
        }

        internal void Add(Archivo UnArchivo)
        {
            string path = Path.Combine(this.Repo, UnArchivo.Name + UnArchivo.Extension);
            File.Create(path).Close(); ;
            
        }

        public void Delete(Archivo archivo)
        {
            string path = Path.Combine(this.Repo, archivo.Name + archivo.Extension);
            File.Delete(path);
        }

        public bool Exists(Archivo archivo)
        {
            string path = Path.Combine(this.Repo, archivo.Name + archivo.Extension);
            return File.Exists(path);
        }

        /*
        public bool ValidExtension(Archivo archivo)
        {
            
            return ((archivo.Extension[0] == '.') && (archivo.Extension.Split('.').Length - 1 == 1));
        }
        */

    }
}
