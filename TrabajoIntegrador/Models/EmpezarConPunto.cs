﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrabajoIntegrador.Models
{
    public class EmpezarConPunto : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string strValue = value as string;
            return strValue.StartsWith(".");
        }
    }
}
