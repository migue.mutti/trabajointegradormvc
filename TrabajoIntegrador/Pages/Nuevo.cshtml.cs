using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TrabajoIntegrador.Models;

namespace TrabajoIntegrador.Pages
{
    public class NuevoModel : PageModel
    {
        [BindProperty]
        public Archivo archivo { get; set; }
        public void OnGet()
        {
            this.archivo = new Archivo();
            this.archivo.Name = "";
            this.archivo.Extension = "";
        }

        public void  OnPostGuardar(Archivo archivo)
        {
            validar();
            if (ModelState.IsValid) 
            { 
                FileManager admArchivo = new FileManager();
                admArchivo.Add(this.archivo);
                ModelState.Clear();
                this.archivo.Name = "";
                this.archivo.Extension = "";
            }
        }
        public void validar()
        {
            if (ModelState.IsValid)
            {
                FileManager admArchivo = new FileManager();
                if (admArchivo.Exists(this.archivo))
                {
                    ModelState.AddModelError("archivo.Name", "el archivo ya existe.");
                }
                /*
                if (!admArchivo.ValidExtension(this.archivo))
                {
                    ModelState.AddModelError("archivo.Extension", "Ingrese una extension correcta.");
                }*/
            }

        }


    }
}
