using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TrabajoIntegrador.Models;

namespace TrabajoIntegrador.Pages
{
    public class EditarModel : PageModel
    {
        [BindProperty]
        public Archivo archivoInicial { get; set; }
        [BindProperty]
        public Archivo archivoModificado { get; set; }
        public void OnGet(string Name, string Extension)
        {
            this.archivoInicial = new Archivo(Name,Extension);
            this.archivoModificado = this.archivoInicial;
        }

        public IActionResult OnPostEditar()
        {
            validar();

            if (ModelState.IsValid)
            {
                    FileManager admArchivo = new FileManager();
                    admArchivo.Edit(this.archivoModificado, this.archivoInicial);
                    return RedirectToPage("Index");
            }
            return Page();
        }

        public void validar()
        {
            if (ModelState.IsValid)
            {
                FileManager admArchivo = new FileManager();
                if (admArchivo.Exists(this.archivoInicial))
                {
                    ModelState.AddModelError("archivoInicial.Name", "el archivo ya existe.");
                }
                /*
                if (!admArchivo.ValidExtension(this.archivoInicial))
                {
                    ModelState.AddModelError("archivoInicial.Extension", "Ingrese una extension correcta.");
                }
                */

            }
            

        }
    }
}
