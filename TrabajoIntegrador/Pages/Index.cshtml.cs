using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TrabajoIntegrador.Models;

namespace TrabajoIntegrador.Pages
{

    public class IndexModel : PageModel
    {
        [BindProperty]
        public List<Archivo> archivos { get; set; }
        public void OnGet()
        {
            FileManager admArchivos = new FileManager();
            this.archivos = admArchivos.getAllFiles();
        }

        public void OnGetDelete(string name, string extension)
        {
            FileManager admArchivos = new FileManager();
            Archivo _archivo = new Archivo();
            _archivo.Name = name;
            _archivo.Extension = extension;
            admArchivos.Delete(_archivo);

            this.archivos = admArchivos.getAllFiles();
        }
        public IActionResult OnGetRename(string name, string extension)
        {
            return RedirectToPage("Editar",new {Name = name, Extension = extension});
        }

        public IActionResult OnGetAdd()
        {

            return RedirectToPage("Nuevo");
        }
    }
}
